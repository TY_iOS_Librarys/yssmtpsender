# YSSMTPSender

[![CI Status](http://img.shields.io/travis/478356182@qq.com/YSSMTPSender.svg?style=flat)](https://travis-ci.org/478356182@qq.com/YSSMTPSender)
[![Version](https://img.shields.io/cocoapods/v/YSSMTPSender.svg?style=flat)](http://cocoapods.org/pods/YSSMTPSender)
[![License](https://img.shields.io/cocoapods/l/YSSMTPSender.svg?style=flat)](http://cocoapods.org/pods/YSSMTPSender)
[![Platform](https://img.shields.io/cocoapods/p/YSSMTPSender.svg?style=flat)](http://cocoapods.org/pods/YSSMTPSender)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

YSSMTPSender is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'YSSMTPSender'
```

## Author

478356182@qq.com, 478356182@qq.com

## License

YSSMTPSender is available under the MIT license. See the LICENSE file for more info.
