//
//  MailService.m
//  STC
//
//  Created by yans on 2017/8/1.
//  Copyright © 2017年 yans. All rights reserved.
//

#import "MailService.h"

#import "NSData+Base64Additions.h"
#import "SKPSMTPMessage.h"

@interface MailService()<SKPSMTPMessageDelegate>
{
    NSMutableArray *mails;
}

@property (nonatomic, assign) BOOL sending;

@end

@implementation MailService

+ (instancetype)sharedInstance
{
    static dispatch_once_t once;
    static id __singleton__;
    dispatch_once( &once, ^{ __singleton__ = [[self alloc] init]; } );
    return __singleton__;
}

- (id)init
{
    if (self = [super init]) {
        mails = [NSMutableArray array];
    }
    return self;
}

+ (void)sendMail:(NSString *)title
         message:(NSString *)content
            file:(NSString *)path
        callback:(SendEmailFinishBlock)callback
{
    [[MailService sharedInstance] sendMail:title message:content file:path callback:callback];
}

+ (void)sendMail:(NSString *)title message:(NSString *)content file:(NSString *)path
{
    [self sendMail:title message:content file:path callback:nil];
}

#pragma mark - SKPSMTPMessage 发送邮件
- (void)sendMail:(NSString *)title message:(NSString *)content file:(NSString *)path callback:(SendEmailFinishBlock)callback
{
//    @synchronized (self) {
        @try {
            
            SKPSMTPMessage *emailMessage = [self createEmailMessage];
            
            emailMessage.subject = title;
            
            NSDictionary *plainPart = @{
                kSKPSMTPPartMessageKey: content,
                kSKPSMTPPartContentTypeKey: @"text/plain",
                kSKPSMTPPartContentTransferEncodingKey: @"8bit",
            };
            
            if (path && ![path isEqualToString:@""] && [[NSFileManager defaultManager] fileExistsAtPath:path]) {
                
                NSData *fileData = [NSData dataWithContentsOfFile:path];
                NSString *fileName = [path lastPathComponent];
                NSString *contentTypeKey = [NSString stringWithFormat:@"text/directory;\r\n\tx-unix-mode=0644;\r\n\tname=\"%@\"", fileName];
                NSString *contentDispositionKey = [NSString stringWithFormat:@"attachment;\r\n\tfilename=\"%@\"", fileName];
                
                NSDictionary *file = @{
                    kSKPSMTPPartContentTypeKey: contentTypeKey,
                    kSKPSMTPPartContentDispositionKey: contentDispositionKey,
                    kSKPSMTPPartMessageKey: [fileData encodeBase64ForData],
                    kSKPSMTPPartContentTransferEncodingKey: @"base64",
                };
                
                emailMessage.parts = @[plainPart, file];
                
            } else {
                emailMessage.parts = @[plainPart];
            }
            
            
            if (!self.sending) {
                self.sending = YES;
                [emailMessage send];
            }
            
            [mails addObject:emailMessage];
        } @catch (NSException *exception) {
            NSLog(@"send email error :%@", exception);
        } @finally {
        }
//    }
}

- (SKPSMTPMessage *)createEmailMessage
{
    SKPSMTPMessage *emailMessage = [[SKPSMTPMessage alloc] init];
    //        _emailMessage.fromEmail = @"rphzty@126.com";
    //        _emailMessage.toEmail = @"rphzty@126.com";
    //        _emailMessage.login = @"rphzty@126.com";
    //        _emailMessage.bccEmail = @"rphzty@126.com";
    //        _emailMessage.pass = @"999999999aaa";
    //        _emailMessage.relayHost = @"smtp.126.com";
    
    emailMessage.fromEmail = @"ios@91118.com";
    emailMessage.toEmail = @"ios@91118.com";
    emailMessage.login = @"ios@91118.com";
    emailMessage.bccEmail = @"ios@91118.com";
    emailMessage.pass = @"Hzty1234";
    emailMessage.relayHost = @"smtp.exmail.qq.com";
    emailMessage.requiresAuth = YES;
    emailMessage.subject = @"";
    emailMessage.wantsSecure = YES;// smtp.gmail.com doesn't work without TLS!
    
    // Only do this for self-signed certs!
    // testMsg.validateSSLChain = NO;
    emailMessage.delegate = self;
    return emailMessage;
}

#pragma mark - SKPSMTPMessageDelegate

- (void)messageSent:(SKPSMTPMessage *)message
{
    [self callSendEmailFinishBlock:YES error:nil message:message];
}

- (void)messageFailed:(SKPSMTPMessage *)message error:(NSError *)error
{
    [self callSendEmailFinishBlock:NO error:error message:message];
}

- (void)callSendEmailFinishBlock:(BOOL)result error:(NSError *)error message:(SKPSMTPMessage *)message
{
    self.sending = NO;
    
    SendEmailFinishBlock callback = (SendEmailFinishBlock)message.userBlock;
    !callback ?: callback(result, message.subject, error);

    [mails removeObject:message];
    if ([mails count] > 0 && !self.sending) {
        SKPSMTPMessage *emailMessage = [mails objectAtIndex:0];
        [emailMessage send];
        self.sending = YES;
    }
}

@end
