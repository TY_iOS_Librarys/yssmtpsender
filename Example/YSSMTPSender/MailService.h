//
//  MailService.h
//  STC
//
//  Created by yans on 2017/8/1.
//  Copyright © 2017年 yans. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^SendEmailFinishBlock)(BOOL, NSString *, NSError *);

@interface MailService : NSObject

+ (instancetype)sharedInstance;

// 发送邮件
+ (void)sendMail:(NSString *)title message:(NSString *)content file:(NSString *)path;

+ (void)sendMail:(NSString *)title message:(NSString *)content file:(NSString *)path callback:(SendEmailFinishBlock)callback;

@end
