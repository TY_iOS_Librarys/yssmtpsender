//
//  YSAppDelegate.h
//  YSSMTPSender
//
//  Created by 478356182@qq.com on 02/13/2018.
//  Copyright (c) 2018 478356182@qq.com. All rights reserved.
//

@import UIKit;

@interface YSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
