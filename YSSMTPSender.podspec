#
# Be sure to run `pod lib lint YSSMTPSender.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = 'YSSMTPSender'
    s.version          = '1.0.4'
    s.summary          = 'Quick SMTP client code for the iPhone.'
    s.license = { :type => 'MIT', :file => 'LICENSE' }
    s.authors = {'yans' => 'aoyan2010@live.cn'}

#    s.platform     = :ios, '7.0'
    s.ios.deployment_target = '7.0'

    s.homepage = 'https://bitbucket.org/TY_iOS_Librarys/yssmtpsender.git'
    s.source = {'git': 'https://bitbucket.org/TY_iOS_Librarys/yssmtpsender.git',:tag => s.version.to_s}


    s.source_files = 'YSSMTPSender/Classes/**/*'

    s.framework = 'CFNetwork'
    s.requires_arc = true

end
